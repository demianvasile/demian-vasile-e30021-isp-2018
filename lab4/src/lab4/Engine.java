
 
package lab4;
class Engine {
            void step(int x,int y ) {
                        start();
                        checkDirectionx(x);
                        
                        execute(x);
                        checkDirectiony(y);
                        execute(y);
                        stop();
            }
 
            private void start(){
                        System.out.println("Start engine.");
            }
            private void stop(){
                        System.out.println("Stop engine.");
            }
 
            private void checkDirectionx(int x){
                        if(x<0)
                                    System.out.println("Moving to the left.");
                        else if(x>0)
                                    System.out.println("Moving to the right.");
            }
 
            
               private void checkDirectiony(int y){
                        if(y<0)
                                    System.out.println("Avans.");
                        else if(y>0)
                                    System.out.println("Retreat.");
            }
            
            
            
            private void execute(int s){
                        System.out.println("Moving "+Math.abs(s)+" steps.");
            }
 
}
 
 