 
package lab4;
import lab4.Robot;
public class BattleField {
            Robot r1;
            Robot r2;
 
            BattleField(){
                        r1 = new Robot();
                        r2 = new Robot();
            }
            public void play(){
                        r1.moveRobot(15,12,5);
                        
                        r2.moveRobot(-10,-8,0);
            }
            public static void main(String[] args){
                        BattleField game = new BattleField();
                        game.play();
            }
}