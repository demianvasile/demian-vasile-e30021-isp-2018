package masina;

public class MasinaV2 {

    String marca;
    int vitezaCurenta;
    CutieViteze cv;

    public MasinaV2(String marca, int vitezaCurenta) {
        this.marca = marca;
        this.vitezaCurenta = vitezaCurenta;
        cv = new CutieViteze();
    }

    void schimbaTreapta(int v) {
        cv.schimbaTreapta(v);
    }

    void accelereaza() {
        if (vitezaCurenta < 140 && cv.citesteTreapta() != 0) {
            vitezaCurenta += 6 - cv.citesteTreapta();
            System.out.println("Viteza curenta: " + vitezaCurenta + " Km\\h");
        } else {
            System.out.println("Eroare accelerare!");
        }
    }

    public void franeaza() {
        for (int i = 0; i < 10; i++) {
            
        
        if (vitezaCurenta > 0) {
            vitezaCurenta -= 1;
        }
        }
        System.out.println("Viteza curenta: " + vitezaCurenta + " Km\\h");

    }
public void oprire(){
vitezaCurenta=0;
 System.out.println("Viteza curenta: " + vitezaCurenta + " Km\\h");
}
    
    
    
    void afiseaza() {
        System.out.println("Masina " + marca + " viteaza = " + vitezaCurenta);
    }

}
