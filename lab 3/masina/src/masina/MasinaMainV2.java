package masina;

import java.util.Scanner;

public class MasinaMainV2 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);

        MasinaV2 m1 = new MasinaV2("Opel",0);
        CutieViteze cv=new CutieViteze();
        
        
        m1.accelereaza();
        m1.accelereaza();
        m1.afiseaza();
        System.out.println("0 - 5 : pentru schimbare viteza \n 6 : acelerare \n 7 : franeaza \n 8: afiseaza detalii masina \n 9 : oprire \n 10: iesire");
        int c = 0;
        while(c!=10){
            c = s.nextInt();
            switch (c){
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    m1.schimbaTreapta(c);
                    break;
                case 6:
                    m1.accelereaza();
                    break;
                case 7:
                    m1.franeaza();                    
                    break;
                case 8:
                    m1.afiseaza();
                    break;
                case 9:
                    m1.oprire();
            }
        }

//        CutieViteze c1 = new CutieViteze();
//        c1.schimbaTreapta(2);
//        c1.schimbaTreapta(6);



    }
}
