package lab.pkg3.senzori;

public class Main {

    public static void main(String[] args) {
	    Senzor s1 = new Senzor();
	    Senzor s2 = new Senzor();
	    Senzor s3; //null

        s1.nume = "Temperatura";
        s1.valoare = 30;

        System.out.println(s1);
        System.out.println(s2);
        //System.out.println(s3); eroare compilare

        System.out.println("Senor s1 = "+s1.nume+" "+s1.valoare);
        System.out.println("Senor s2 = "+s2.nume+" "+s2.valoare);

        s2 = s1;

        System.out.println("Senor s1 = "+s1.nume+" "+s1.valoare);
        System.out.println("Senor s2 = "+s2.nume+" "+s2.valoare);

        s1.valoare = 45;
        System.out.println(s2.valoare);

        s3 = new Senzor();

        //////////////////////////////////

        SenzorV2 p1 = new SenzorV2();
        p1.nume = "Umiditate";
        for(int i=0;i<10;i++)
            p1.inc();

        p1.afiseaza();

        System.out.println("Valoare senzor p1 "+p1.citesteValoare());
        SenzorV2 p2 = new SenzorV2();
        p2.afiseaza();

        //////////////////////////////////

        Senzor s4 = new Senzor();
        s4.nume = "Proximitate";
        s4.valoare = 1;

        SenzorV3 q1 = new SenzorV3("Proximitate",1);
        SenzorV3 q2 = new SenzorV3();

        System.out.println(q1);
        System.out.println(q2);
    }
}