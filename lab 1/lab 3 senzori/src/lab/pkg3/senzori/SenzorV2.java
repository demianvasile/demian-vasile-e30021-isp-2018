package lab.pkg3.senzori;

public class SenzorV2 {
    String nume;
    int valoare;

    void inc(){
        valoare++;
        System.out.println("Senzor "+nume+" valoare incerementata "+valoare);
    }

    int citesteValoare(){
        return valoare;
    }

    void afiseaza(){
        System.out.println("Senzor "+nume+" valoare "+valoare);
    }
}