
  package lab.pkg3.senzori;


public class SenzorV3 {
    //atribute
    String nume;
    int valoare;

    //constructori
    SenzorV3(){
        nume = "Temperatura";
        valoare = 0;
    }

    SenzorV3(String nume, int valoare){
        this.nume = nume;
        this.valoare = valoare;
    }

    //metode
    public String toString(){
        return "Senzor "+nume+" valoare="+valoare;
    }


}
