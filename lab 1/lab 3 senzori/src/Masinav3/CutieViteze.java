package Masinav3;

    public class CutieViteze {

        int treaptaCurenta;

        CutieViteze() {
            treaptaCurenta = 0;
        }

        void schimbaTreapta(int treapta) {
            System.out.println("Schimba treapta din " + treaptaCurenta + " in " + treapta);
            if (treapta >= 0 && treapta <= 5) {
                treaptaCurenta = treapta;
            } else {
                System.out.println("Eroare schimbare. Treapta invalida.");
            }
        }

        int citesteTreapata() {
            return treaptaCurenta;
        }

    }