  package Masinav3;

public class MasinaV2 {

    String marca;
    int vitezaCurenta;
    CutieViteze cv;

    public MasinaV2(String marca, int vitezaCurenta) {
        this.marca = marca;
        this.vitezaCurenta = vitezaCurenta;
        cv = new CutieViteze();
    }

    void schimbaTreapta(int v){
        cv.schimbaTreapta(v);
    }

    void accelereaza(){
        if(vitezaCurenta<140&&cv.citesteTreapata()!=0) {
            vitezaCurenta++;
            System.out.println("Viteza curenta: "+vitezaCurenta+" Km\\h");
        }else{
            System.out.println("Eroare accelerare!");
        }
    }

    public void franeaza() {
        vitezaCurenta--;
    }

    void afiseaza(){
        System.out.println("Masina "+marca+" viteaza = "+vitezaCurenta);
    }


}  

