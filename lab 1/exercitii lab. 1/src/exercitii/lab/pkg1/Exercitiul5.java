package exercitii.lab.pkg1;

import java.util.Random;

public class Exercitiul5
{

    public static void main(String[] args)
    {
        int vec[] = genVector();

        System.out.println("vector generat");
        for (int i = 0; i < 10; i++)
        {
            System.out.print(vec[i] + " ");
        }
        sortareBubble(vec);
        System.out.println("");
        System.out.println("vector sortat (metoda Bubble)");
        for (int i = 0; i < 10; i++)
        {
            System.out.print(vec[i] + " ");
        }

    }

    public static void sortareBubble(int[] vector)
    {
        int dimensiuneVector = vector.length;
        int temporar = 0;

        for (int i = 0; i < dimensiuneVector; i++)
        {
            for (int j = 1; j < (dimensiuneVector - i); j++)
            {
                if (vector[j - 1] > vector[j])
                {
                    temporar = vector[j - 1];
                    vector[j - 1] = vector[j];
                    vector[j] = temporar;
                }
            }
        }
    }

    public static int[] genVector()
    {
        int[] A = new int[10];
        Random rand = new Random();

        for (int i = 0; i < 10; i++)
        {
            A[i] = rand.nextInt(20) + 1;
        }
        return A;
    }

}
