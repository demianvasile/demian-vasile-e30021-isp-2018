package exercitii.lab.pkg1;

import java.util.Scanner;

public class Exercitiul4
{

    public static void main(String[] args)
    {
        int nrElemente;
        int maxNr = 0;
        Scanner scan = new Scanner(System.in);

        System.out.print("numarul de elemente al vectorului A ");
        nrElemente = scan.nextInt();

        int A[] = new int[nrElemente];

        for (int i = 0; i < nrElemente; i++)
        {
            System.out.print("A[" + i + "] = ");
            A[i] = scan.nextInt();
        }

        for (int i = 0; i < A.length; i++)
        {
            if (A[i] > maxNr)
            {
                maxNr = A[i];
            }
        }

        System.out.print("cel mai mare este " + maxNr + ".");

    }

}
