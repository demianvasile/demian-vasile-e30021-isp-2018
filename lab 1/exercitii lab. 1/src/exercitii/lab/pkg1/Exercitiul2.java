package exercitii.lab.pkg1;

import java.util.Scanner;

public class Exercitiul2
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.print("1 la 9 (IF) : ");
        System.out.println(printNumberIf(scan.nextInt()));

        System.out.print("1 la 9 (SWICH) : ");
        System.out.println(printNumberIf(scan.nextInt()));
    }

    public static String printNumberIf(int nr)
    {
        if (nr == 0)
        {
            return "Zero";
        }
        else if (nr == 1)
        {
            return "One";
        }
        else if (nr == 2)
        {
            return "Two";
        }
        else if (nr == 3)
        {
            return "Three";
        }
        else if (nr == 4)
        {
            return "Four";
        }
        else if (nr == 5)
        {
            return "Five";
        }
        else if (nr == 6)
        {
            return "Six";
        }
        else if (nr == 7)
        {
            return "Seven";
        }
        else if (nr == 8)
        {
            return "Eight";
        }
        else if (nr == 9)
        {
            return "Nine";
        }
        else
        {
            return "Other";
        }
    }

    public static String printNumberSwich(int nr)
    {
        switch (nr)
        {
            case 0:
                return "Zero";
            case 1:
                return "One";
            case 2:
                return "Two";
            case 3:
                return "Three";
            case 4:
                return "Four";
            case 5:
                return "Five";
            case 6:
                return "Six";
            case 7:
                return "Seven";
            case 8:
                return "Eight";
            case 9:
                return "Nine";
            default:
                return "Other";
        }

    }

}
