package exercitii.lab.pkg1;

import java.util.Scanner;

public class Exercitiul3
{

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int A, B; //capetele listei
        boolean prim = true;
        int totalNrPrime = 0;

        System.out.print("primul numar(A) ");
        A = scan.nextInt();
        System.out.print("ultimul numar(B) ");
        B = scan.nextInt();

        System.out.print("numerele prime ");
        for (int i = A; i <= B; i++)
        {
            prim = true;
            for (int j = 2; j < i / 2; j++)
            {
                if (i % j == 0)
                {
                    prim = false;
                }
            }
            if (prim == true && i != 1)
            {
                System.out.print(i + " ");
                totalNrPrime++;
            }
        }
        if (totalNrPrime == 0)
        {

            System.out.print("nu exista numere prime");
        }
        else
        {
            System.out.println("sunt " + totalNrPrime + " numere prime");
        }
    }
}
