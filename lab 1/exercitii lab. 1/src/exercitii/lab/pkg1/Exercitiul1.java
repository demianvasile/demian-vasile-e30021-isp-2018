package exercitii.lab.pkg1;

import java.util.Scanner;

public class Exercitiul1 {

    public static void main(String[] args) 
    {
        int n1,n2;
        
        Scanner citire = new Scanner(System.in);
        
        System.out.print("N1 = ");
        n1 = citire.nextInt();
        
        System.out.print("N2 = ");
        n2 = citire.nextInt();
        
        if(n1>n2)
        {
            System.out.println("N1 (" + n1 + ") > N2 (" + n2 + ")");
        }
        else if(n1 < n2)
        {
            System.out.println("N1 (" + n1 + ") < N2 (" + n2 + ")");
        }
        else
        {
            System.out.println("N1 = N2 = " + n1);
        }
    }        
}
    

