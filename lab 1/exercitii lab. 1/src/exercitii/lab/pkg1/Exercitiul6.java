package exercitii.lab.pkg1;

import java.util.Scanner;

public class Exercitiul6
{

    public static void main(String[] args)
    {
        int nr;
        Scanner citire = new Scanner(System.in);

        System.out.print("un numar ");
        nr = citire.nextInt();

        System.out.println("factorial " + nr + " prin metoda recursiva " + factorialRecursiv(nr));
        System.out.println("factorial " + nr + " prin metoda nerecursiva " + factorialNerecursiv(nr));
    }

    public static int factorialRecursiv(int nr)
    {
        int rezultat;

        if (nr == 0 || nr == 1)
        {
            return 1;
        }

        rezultat = factorialRecursiv(nr - 1) * nr;

        return rezultat;
    }

    public static int factorialNerecursiv(int nr)
    {
        int rezultat = 1;

        for (int i = 1; i <= nr; i++)
        {
            rezultat = rezultat * i;
        }
        return rezultat;
    }

}
